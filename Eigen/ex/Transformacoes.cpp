#include <iostream>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>


using namespace Eigen;
using namespace cv;

int main(int argc, char* argv[]) {

    int ret_x0 = 50, ret_y0 = 50, comprimento = 20;
    double angulo_radianos = 15.0;
    double escala_x0 = 30.0, escala_y0 = 30.0;
    angulo_radianos *= 3.1415 / 180;

	Mat imagem = imread("1.jpg");
	if ( imagem.empty() ){
		std::cerr << "Imagem não encontrada." << std::endl;
		exit(-1);
	}

  /* Declaração das imagens operacionadas */
  Mat recorte = Mat::zeros(imagem.rows, imagem.cols, CV_8UC3);
  Mat transladada = Mat::zeros(imagem.rows, imagem.cols, CV_8UC3);
  Mat rotacionar = Mat::zeros(imagem.rows, imagem.cols, CV_8UC3);
  Mat escalonada = Mat::zeros(imagem.rows*escala_x0, imagem.cols*escala_y0, CV_8UC3);
  Mat all = Mat::zeros(imagem.rows*escala_x0, imagem.cols*escala_y0, CV_8UC3);

  int x_total = ret_x0 + comprimento;
  int y_total = ret_y0 + comprimento;

  for(int l=ret_x0;l<x_total;l++) {
    for(int c=ret_y0;c<y_total;c++) {
      // Cor do pixel na posição original
      Vec3b p = imagem.at<Vec3b>(l,c);

      // Posição do pixel na imagem original
      Vector3d p1(c, l, 1);

      // Auxiliar para calcular posições
      Vector3d d1;

      // Para mostrar o quadrado recortado da imagem, simplesmente manteremos o mesmo
      // ponto, exceto que ele será colocado numa imagem de fundo todo preto.
      recorte.at<Vec3b>(l,c) = p;

      // Calcula onde fica os novos pontos
      Matrix3d deslocamento;
      deslocamento << 1, 0, 0, 0, 1, 0, -ret_x0, -ret_y0, 1;
      d1 = p1.transpose() * deslocamento;

      transladada.at<Vec3b>(d1(1,0), d1(0,0)) = p;

      // Calcula onde fica os novos pontos
      Matrix3d matriz_giro;
      matriz_giro << cos(angulo_radianos), sin(angulo_radianos), 0, -sin(angulo_radianos), cos(angulo_radianos), 0, 0, 0, 1;
      d1 = p1.transpose() * matriz_giro;

      // Isso faz com que pixels vazios entre os pixels coloridos sejam
      // preenchidos
      rotacionar.at<Vec3b>(d1(1,0), d1(0,0) -1) = p;
      rotacionar.at<Vec3b>(d1(1,0), d1(0,0) +1) = p;
      rotacionar.at<Vec3b>(d1(1,0) -1, d1(0,0)) = p;
      rotacionar.at<Vec3b>(d1(1,0) +1, d1(0,0)) = p;

      // Calcula onde fica os novos pontos
      Matrix3d matriz_escala;
      matriz_escala << escala_x0, 0, 0, 0, escala_y0, 0, 0, 0, 1;
      d1 = p1.transpose() * matriz_escala;

      // Preencimento
      for(int i = 0;i<escala_y0;i++)
        for(int j = 0;j<escala_x0;j++)
          escalonada.at<Vec3b>(d1(1,0) + i, d1(0,0) + j) = p;

      // Calcula onde fica os novos pontos
      deslocamento << 1, 0, 0, 0, 1, 0, -ret_x0*escala_x0, -ret_y0*escala_y0, 1;
      d1 = p1.transpose() * matriz_escala * deslocamento * matriz_giro;

      // Preenchimento

      for(int k = 0;k<=escala_y0;k++)
        for(int j = 0;j<=escala_x0;j++)
          all.at<Vec3b>(d1(1,0)+k, d1(0,0) + j) = p;

      for(int k = escala_y0;k>=0;k--)
        for(int j = escala_x0;j>=0;j--)
          if((d1(0,0) - j) >= 0 && (d1(1,0) - k) >= 0)
            all.at<Vec3b>(d1(1,0)-k, d1(0,0) - j) = p;

      for(int k = escala_x0;k>=0;k--)
        for(int i = escala_y0;i>=0;i--)
          if((d1(1,0) - i) >= 0 && (d1(0,0) - k) >= 0)
            all.at<Vec3b>(d1(1,0) - i, d1(0,0) - k ) = p;

      for(int k = 0;k<=escala_x0;k++)
        for(int i = 0;i<=escala_y0;i++)
          all.at<Vec3b>(d1(1,0) + i, d1(0,0) + k) = p;
    }
  }

  namedWindow("Recortada", WINDOW_NORMAL);
  imshow("Recortada", recorte);
  namedWindow("Transladada", WINDOW_NORMAL);
  imshow("Transladada", transladada);
  namedWindow("Rotacionar", WINDOW_NORMAL);
  imshow("Rotacionar", rotacionar);
  namedWindow("Escalonada", WINDOW_NORMAL);
  imshow("Escalonada", escalonada);
  namedWindow("Todas as transformações", WINDOW_NORMAL);
  imshow("Todas as transformações", all);

  waitKey(0);
  return 0;
}
