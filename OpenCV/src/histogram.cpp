
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
using namespace std;
using namespace cv;

#define w 512
#define h 400
void MyLine( Mat img, Point start, Point end );
RNG rng( 0xFFFFFFFF );

int main(int argc, char** argv)
{
    CommandLineParser parser( argc, argv, "{@input | ../imagens/teste.png | input image}" );
    Mat src = imread( samples::findFile( parser.get<String>( "@input" ) ), IMREAD_COLOR );
    if( src.empty() )
    {
        return EXIT_FAILURE;
    }
    vector<Mat> bgr_planes;
    split( src, bgr_planes );
    int histSize = 256;
    float range[] = { 0, 256 }; //the upper boundary is exclusive
    const float* histRange = { range };
    bool uniform = true, accumulate = false;
    Mat b_hist, g_hist, r_hist;
    calcHist( &bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );
    int hist_w = 15*w/16, hist_h = 15*h/16 - 3;
    int bin_w = cvRound( (double) hist_w/histSize );
    Mat histImage( h, w, CV_8UC3, Scalar( 0,0,0) );
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    for( int i = 1; i < histSize; i++ )
    {
        line( histImage, Point( 3+bin_w*(i-1)+ w/16, hist_h - cvRound(b_hist.at<float>(i-1)) ),
              Point( 3+bin_w*(i)+ w/16, hist_h - cvRound(b_hist.at<float>(i)) ),
              Scalar( 255, 0, 0), 2, 8, 0  );
        line( histImage, Point( 3+bin_w*(i-1)+ w/16, hist_h - cvRound(g_hist.at<float>(i-1)) ),
              Point( 3+bin_w*(i)+ w/16, hist_h - cvRound(g_hist.at<float>(i)) ),
              Scalar( 0, 255, 0), 2, 8, 0  );
        line( histImage,
             Point( 3+bin_w*(i-1)+ w/16, hist_h - cvRound(r_hist.at<float>(i-1)) ),
              Point( 3+bin_w*(i)+ w/16, hist_h - cvRound(r_hist.at<float>(i)) ),
              Scalar( 0, 0, 255), 2, 8, 0  );

    }
    //Grafico X-Y
    MyLine( histImage, Point( w/16, 15*h/16 ), Point( 15*w/16, 15*h/16 ) ); // X-Linha
    MyLine( histImage, Point( w/16, h/16 ), Point( w/16, 15*h/16 ) ); //Y-Linha
    putText( histImage, "X", Point(15*w/16,63*h/64), FONT_HERSHEY_COMPLEX, 0.5,
           Scalar(150, 150, 150), 1.25, 64 );
    putText( histImage, "Y", Point(w/64, h/16), FONT_HERSHEY_COMPLEX, 0.5,
           Scalar(150, 150, 150), 1.25, 64 );
    putText( histImage, "(0,0)", Point(w/64, 63*h/64), FONT_HERSHEY_COMPLEX, 0.5,
           Scalar(150, 150, 150), 1.25, 64 );
    /*MyLine( histImage, Point( w/4, 7*w/8 ), Point( w/4, w ) );*/
    //imshow("Source image", src );
    imshow("calcHist Demo", histImage );
    waitKey();
    return EXIT_SUCCESS;
}
void MyLine( Mat img, Point start, Point end )
{
  int thickness = 2;
  int lineType = LINE_8;
  line( img,
    start,
    end,
    Scalar( 220, 220, 220 ),
    thickness,
    lineType );
}
