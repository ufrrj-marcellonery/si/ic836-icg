# Tarefa sobre OpenGL

Olhar os arquivos no repositório OpenGL e comenta-los;<br/>
Fazer modificações nos arquivos exemplo para poder girar, scalonar e transladar os objetos desenhados;<br/>
Compilar o jogo e comentar seu funcionamento, analisar o código e fazer um comentário de que pode ser melhorado.<br/>
<br/>

#Comentários

## exemplo1.c

Desenha um Quadrado, mas só aparece 2 das arestas do quadrados que são paralelas ao eixo y. (Só as arestas) <br/>
`glBegin(GL_LINES);
		glVertex2i(100,150);
  		glVertex2i(100,100);
  		glColor3f(0.0f, 0.0f, 1.0f); // Especifica a cor corrente: Azul
  		glVertex2i(150,100);
  		glVertex2i(150,150);
glEnd();`<br/>
GL_LINES especifica que vão ser gerados linhas a partir dos pontos. Logo, pegando os dois pontos de Vertex2i e gerando uma primeira linha, depois gerando outra linha com os outros pontos.

## exemplo2.c

Desenha um Quadrado, mas só aparece 3 das arestas do quadrados. Umas das arestas que são paralelas ao eixo x (a aresta superior), não aparece. (Só as arestas)<br/>
`glBegin(GL_LINES_STRIP);
		glVertex2i(100,150);
  		glVertex2i(100,100);
  		glColor3f(0.0f, 0.0f, 1.0f); // Especifica a cor corrente: Azul
  		glVertex2i(150,100);
  		glVertex2i(150,150);
glEnd();`<br/>
GL_LINES_STRIP especifica que vai ser gerado uma linha continua a partir dos pontos. Logo, pegando os dois pontos de Vertex2i e gerando uma primeira linha, depois gerando uma continuação da linha pegando o segundo ponto mais próximo, e em seguida, gerando outra linha a partir deste último ponto.<br/>
A->B->C->D : Mas A não liga diretamente com C ou D e B não liga com D. 

## exemplo3.c

Desenha um Quadrado com todas as arestas. (Só as arestas)<br/>
`glBegin(GL_LINES_LOOP);
		glVertex2i(100,150);
  		glVertex2i(100,100);
  		glColor3f(0.0f, 0.0f, 1.0f); // Especifica a cor corrente: Azul
  		glVertex2i(150,100);
  		glVertex2i(150,150);
glEnd();`<br/>
GL_LINES_LOOP especifica que vai ser gerado uma linha continua a partir dos pontos. Logo, pegando os dois pontos de Vertex2i e gerando uma primeira linha, depois gerando uma continuação da linha pegando o segundo ponto mais próximo, e em seguida, gerando outra linha a partir deste último ponto. E a partir do último ponto, ligando ele ao primeiro ponto. Formando, assim, um quadrado.<br/>
A->B->C->D->A. 

## exemplo4.c

Desenha um Triângulo com preenchimento.<br/>
`glBegin(GL_TRIANGLES);
		glVertex2i(100,150);
  		glVertex2i(100,100);
  		glColor3f(0.0f, 0.0f, 1.0f); // Especifica a cor corrente: Azul
  		glVertex2i(150,100);
glEnd();`<br/>
GL_Triangles especifica que vai ser gerado um triângulo a partir dos pontos. Logo, pegando os dois pontos de Vertex2i e gerando uma primeira linha, depois gerando uma continuação da linha pegando o segundo ponto, e em seguida, gerando outra linha a partir deste último ponto conectando ao primeiro.<br/> Após isso, o triângulo é preenchido com a cor expecificada.

## exemplo10.c

Desenha um Quadrdo com preenchimento.<br/>
`glBegin(GL_QUADS);
		glVertex2i(100,150);
  		glVertex2i(100,100);
  		glColor3f(0.0f, 0.0f, 1.0f); // Especifica a cor corrente: Azul
  		glVertex2i(150,100);
  		glVertex2i(150,150);
glEnd();`<br/>
GL_LINES_LOOP especifica que vai ser gerado uma linha continua a partir dos pontos. Logo, pegando os dois pontos de Vertex2i e gerando uma primeira linha, depois gerando uma continuação da linha pegando o segundo ponto mais próximo, e em seguida, gerando outra linha a partir deste último ponto. E a partir do último ponto, ligando ele ao primeiro ponto. Formando, assim, um quadrado.<br/>
A->B->C->D->A. E depois de feito o quadrado, ele é preenchido com a cor especificada.