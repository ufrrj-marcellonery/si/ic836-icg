#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#define PI          3.1415
#define	WIDTH		500
#define	HEIGHT	    500


//Coordinate system
float Xmin=-8, Xmax=8, Ymin=-8, Ymax=8;
int *lado= 0;

void setupmywindow()
{
    glClearColor(0,0,0,0);
    gluOrtho2D(Xmin, Xmax, Ymin, Ymax);
}


void mypolygon(float radius)
{

    glColor3f(1,0,0);
    int numPoints=*lado;
    float x,y;
    float centerx,centery=0;
    glBegin(GL_POLYGON);

    for (int i = 0; i < numPoints; i++){
    x = centerx + radius * sin(2.0*PI*i/numPoints);
    y = centery + radius * cos(2.0*PI*i/numPoints);
    glVertex2f(x, y);
}
    glEnd();

}

void myDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT);
    mypolygon(2.0);
    glutSwapBuffers();
}

int main(int argc, char **argv)
{
    int valor = 0;
    while (valor < 3){
    printf("\n Digite o numero de lados (maior que 2): ");
    scanf("%i", &valor);
    }
    lado = &valor;

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutInitWindowSize(WIDTH,HEIGHT);
    glutCreateWindow("Poligonos Regulares");
    setupmywindow();
    glutDisplayFunc(myDisplay);
    glutMainLoop();
}

